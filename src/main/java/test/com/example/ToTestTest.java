package test.com.example;

import example.ToTest;
import org.junit.Test;

import java.util.Objects;

/** 
* ToTest Tester. 
* 
* @author <Authors name> 
* @since <pre>????? 9, 2022</pre> 
* @version 1.0 
*/ 
public class ToTestTest { 


/** 
* 
* Method: toTest(String a) 
* 
*/ 
@Test
public void testToTest() throws Exception {
    String a = "ez";
    assert Objects.equals(new ToTest().toTest(a), a);
} 


} 
